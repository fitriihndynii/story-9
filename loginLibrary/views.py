from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import MakeUser
from django.contrib.auth.models import User


# Create your views here.
def fitribookstore(request):
    return render(request, 'fitribookstore.html')

def loginLibrary(request):
    if request.method == "POST":
        username_input = request.POST['username']
        password_input = request.POST['password']
        user = authenticate(username=username_input, password=password_input)
        if user is not None:
            login(request, user)
            return redirect('fitribookstore')
        else:
            return render(request, 'login.html', {'error_message':'Masukkan tidak valid'})

    return render(request, 'login.html')

def register(request):
	if request.method =='POST':
		form = MakeUser(request.POST)
		if form.is_valid():
			if form.cleaned_data['password']!= form.cleaned_data['konfirmasi_password']:
				return render(request, 'register.html', {'form':form,
														'error_message':'Password tidak sama'})
			else:
				user = User.objects.create_user(
					form.cleaned_data['username'],
					form.cleaned_data['email'],
					form.cleaned_data['password']
					)
				user.save()
				return render(request, 'register.html', {
                    'form': form,
                    'message': 'Akun berhasil dibuat, Silahkan Login.'
                })
	else:
		form = MakeUser()
	isi = {'form': form,}
	return render(request, 'register.html', isi)


def logout_view(request):
    logout(request)
    return redirect('loginLibrary')
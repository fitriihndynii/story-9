from django.contrib import admin
from django.urls import path
from . import views

#create your urls here
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.loginLibrary, name='loginLibrary'),
    path('logout/', views.logout_view, name='out'),
    path('fitribookstore/', views.fitribookstore, name='fitribookstore'),
    path('register/', views.register, name='register'),
]
from django.apps import AppConfig


class LoginlibraryConfig(AppConfig):
    name = 'loginLibrary'

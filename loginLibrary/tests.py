from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import loginLibrary, fitribookstore, register, logout_view
from .forms import MakeUser
from django.contrib.auth.models import User
from django.contrib import auth

# Create your tests here.
class TestUrldanViews(TestCase):
    def test_apakah_terdapat_url_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_fungsi_login_dijalankan(self):
        response = resolve(reverse('loginLibrary'))
        self.assertEqual(response.func, loginLibrary)

    def test_apakah_template_html_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'login.html')

    def test_apakah_terdapat_form_login(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('form-group', content)

    def test_apakah_fungsi_fitribookstore_dijalankan(self):
        response = resolve(reverse('fitribookstore'))
        self.assertEqual(response.func, fitribookstore)

class TestRegisterLogin(TestCase): 
	@classmethod
	def setUpTestData(cls):
		user = User.objects.create_user(username='fitriihndynii', email='fitriihndynii@gmail.com', password='fitriihndynii')
		user.save()

	def test_apakah_login(self):
		response = self.client.post('', data={'username':'fitriihndynii', 'password':'fitriihndynii'})
		exist = auth.get_user(self.client)
		self.assertTrue(exist.is_authenticated)

		html_response = response.content.decode('utf8')
		self.assertIn(html_response, 'Hello fitriihndynii!')
